import java.time.Duration
import java.util.Collections.singletonList
import java.util.Properties

import org.apache.kafka.clients.consumer.KafkaConsumer

import scala.collection.JavaConverters._

object Consumer {
  def main(args: Array[String]): Unit = {

    val consumer = new KafkaConsumer(props)

    val topic = "practiceJSON"
    val wordsToFilter = Seq("gov", "Penddreth")

    try {
      consumer.subscribe(singletonList(topic))
      while (true) {
        val record = consumer.poll(Duration.ofMillis(100))
        for (r <- record.asScala) {
          val value = wordsToFilter.fold(r.value)(_.replace(_, ""))
          println(s"El topic '${r.topic()}' ha recibido un mensaje: Key = '${r.key}', Value = '$value'")
        }
      }
    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      consumer.close()
    }
  }
  private def props: Properties = {
    val p = new Properties()
    p.put("group.id", "practice")
    p.put("bootstrap.servers", "localhost:9092")
    p.put("key.deserializer", "org.apache.kafka.common.serialization.IntegerDeserializer")
    p.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    p
  }
}
