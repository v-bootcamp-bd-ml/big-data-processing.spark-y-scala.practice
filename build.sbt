name := "Práctica"

version := "0.1"

scalaVersion := "2.12.10"

val sparkVersion = "2.4.5"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core",
  "org.apache.spark" %% "spark-streaming",
  "org.apache.spark" %% "spark-sql",
  "org.apache.spark" %% "spark-streaming-kafka-0-10"
).map(_ % sparkVersion)

val circeVersion = "0.12.3"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)
