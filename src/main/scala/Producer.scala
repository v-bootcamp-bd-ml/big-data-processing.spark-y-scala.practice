import java.util.Properties

import io.circe._
import io.circe.parser._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.spark.sql.SparkSession

object Producer {
  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder()
      .appName("json")
      .master("local[*]")
      .getOrCreate()

    val df = spark.read.option("multiline", "true").json("personal.json")

    val producer = new KafkaProducer[Int, String](props)
    val topic = "practiceJSON"

    try {
      df.toJSON
        .collect()
        .foreach(person => {
          val json = parse(person).getOrElse(Json.Null)
          val id = json.hcursor.get[Int]("id").getOrElse(0)
          println(s"Enviando al topic '$topic' un mensaje: Key = '$id', Value = '$person'")
          val record = new ProducerRecord(topic, id, person)
          producer.send(record)
          ()
        })
    } catch {
      case ex: Exception => ex.printStackTrace()
    } finally {
      producer.close()
      spark.stop()
    }
  }

  private def props: Properties = {
    val p = new Properties()
    p.put("bootstrap.servers", "localhost:9092")
    p.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer")
    p.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    p.put("acks", "all")
    p
  }
}
