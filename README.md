# Explicación de la práctica
## Uso de Kafka desde la línea de comandos
En primer lugar creamos el topic que vamos a utilizar, en este caso `practice`. Para ello utilizamos el comando `kafka-topic.sh`, que también utilizamos a continuación para confirmar la correcta creación del mismo.

![Screen1](images/Screen1.png)

Posteriormente, deseamos verificar que es posible enviar el contenido de un fichero como un mensaje a un topic de Kafka y que otro proceso diferente lo lea.

El envío lo realizaremos mediante el comando `kafka-console-producer.sh` y la lectura mediante el comando `kafka-console-consumer.sh`.

En primer lugar, en un terminal nos subscribimos al topic en cuestión:

![Screen2](images/Screen2.png)

y en otro terminal diferente enviamos el contenido del fichero al topic:

![Screen3](images/Screen3.png)

casi inmediatamente veremos que el primer terminal ha recibido, efectivamente, el contenido del fichero:

![Screen4](images/Screen4.png)

## Uso de Kafka desde Scala con Spark
Este mismo proyecto contiene el código fuente con el que se ha realizado esta parte de la práctica.

Se adjuntan capturas de pantalla del `Producer`:

![Producer](images/Producer.png)

y del `Consumer`:

![Consumer](images/Consumer.png)

En este último se ha decidido filtrar las palabras `gov` y `Penddreth`.

## Instalación de Apache Zeppelin
En primer lugar, en la máquina virtual utilizada durante el curso, descargamos el paquete binario desde la [página oficial](http://zeppelin.apache.org/download.html).

Una vez descargado, procedemos a descomprimir el fichero con ayuda del comando `tar`, creamos un enlace simbólico para facilitar su uso con `ln` e iniciamos el servicio correspondiente:

![Zeppelin1](images/Zeppelin1.png)

A continuación navegamos a la dirección http://localhost:8080 donde tendremos acceso al interface web de **Zeppelin**:

![Zeppelin2](images/Zeppelin2.png)

## Práctica realizada en Apache Zeppelin

En este interface web abierto en el punto anterior, únicamente necesitamos hacer _click_ sobre `Create new note` para que nos aparezca el diálogo de creación de un nuevo _notebook_.

Introducimos en él el nombre deseado y el interprete a utilizar por defecto (`Práctica` y `spark` en nuestro caso) y pulsamos el botón `Create`:

![Zeppelin3](images/Zeppelin3.png)

**Zeppelin** nos muestra el _notebook_ recién creado completamente vacío.

Únicamente necesitamos introducir el código deseado en el párrafo ya creado y pulsar sobre el botón `Run` (mostrado como una punta de flecha hacia la derecha) o pulsar en el teclado `Shift+Enter`:

![Zeppelin4](images/Zeppelin4.png)

El código utilizado es el siguiente:
```scala
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructType}

val spark = SparkSession.builder().appName("zeppelin").master("local[*]").getOrCreate()

val schema = new StructType()
  .add("id", IntegerType, nullable = true)
  .add("nombre", StringType, nullable = true)
  .add("edad", IntegerType, nullable = true)
  .add("points", IntegerType, nullable = true)

val df = spark.read.option("delimiter", ",").schema(schema).csv("IdeaProjects/Práctica/amigos.csv")

df.createOrReplaceTempView("amigos")

val df1 = spark.sql("select count(*) from amigos")

df1.show(1)

spark.stop
```
